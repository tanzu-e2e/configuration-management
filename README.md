## Demonstrate a GitOps configuration managment pattern along with spring music app

This is a simple example of using kapp controller, kubernetes secrets and Spring to manage a common set of configuration across multiple environments.  In the past we may have used the Spring Config server to achieve this pattern, but kubernetes has native capabilites to support environment provided configuration.  Also, important to note this is not unique to Java/Spring, these secrets can be mounted into any pod as a volume and achieve similar capability, although Spring makes dynamic reloading very easy.

### Install Kapp controller

Install Kapp controller in your cluster.  This is the controller that will monitor your state (Git, Helm, etc) and apply changes.  

```bash

$ kubectl apply -f https://github.com/vmware-tanzu/carvel-kapp-controller/releases/latest/download/release.yml

```

### Deploy Kapp App Resources

With the spring music demo in this Gitlab Group we deploy 3 versions of spring music into a dev/test/prod namespace.  To support that we create one Kapp App per namespace.  This approach can be modified to work accross multiple clusters, environments, etc.  This just supports the demo use case.

```bash

#these commands will create the common config Kapp App in each namespace for the demo scenario (dev, test, prod are namespaces spring music is running in)
#could integrate the initial deployment into GitLab, and probably will at some point
./deploy.sh dev
./deploy.sh test
./deploy.sh prod

```

### Show how Spring music uses the common config

Spring music is looking for a config map named common-config, it will poll for it so it will dynamically respond to it being created or changed.  Open a given instance in a browser and click the info icon in the top right.

![example](docs/config.png)

you will notice that values from the common config display here.

### Change the config

Now you can run a merge request flow or just edit the appropriate secret in base-config from the GitLab UI and the spring music app will respond to show the updated values.  This is meant to demonstrate how configuration can be managed independently of the app deployment process (and support https://12factor.net/config).  This config would likely be managed by the DevOps or Platform team and exposed as a service to dev teams for common attributes with environmental nuances.
